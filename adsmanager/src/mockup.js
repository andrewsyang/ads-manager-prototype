function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    url = url.toLowerCase(); // This is just to avoid case sensitiveness  
    name = name.replace(/[\[\]]/g, "\\$&").toLowerCase();// This is just to avoid case sensitiveness for query parameter name
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

$( document ).ready(function() {
  
  //showPanel('edit-adgroup-multiple');
  
  if (getParameterByName('page') == "error") {
    $("#chart-content").remove();
    $("#root").load("page-error.html");
    return;
  }
  
  if (getParameterByName('intro') == "true") {
    showModal('intro-1');
  }
  
  // Load the header
  $("#header-content").load("header-account.html");
  
  // Load the chart
  if (getParameterByName('page') != "policy-violation") {
    $("#chart-content").load("chart-account.html");
    countUp();
  } else {
    showModal('violation');
    $("#chart-content").load("chart-account-callout.html");
    countUp();
  }
  
  // Load the subnav
  $("#subnav-content").load("subnav-account.html");
  
  // Load the table
  switch(getParameterByName('page')) {
    case "table-error":
      $("#table-content").load("table-error.html", function(){
        countUp();
      });
      break;
    case "table-empty":
      $("#chart-content").load("chart-account-loading.html");
      $("#table-content").load("table-empty.html", function(){
        countUp();
      });
      break;  
    case "chart-loading":
      $("#chart-content").load("chart-account-loading.html");
      $("#table-content").load("table-account-campaigns.html", function(){
        countUp();
      });
      break;    
    default:
      $("#table-content").load("table-account-campaigns.html", function(){
        countUp();
      });
      //$("#table-content").load("table-campaign-audience.html");
  }
  
  // $("#table-content").load("table-account-campaigns-daily.html", function(){
  //   countUp();
  // });
  
  // Tab behaviors
  $(document).on("click", ".TabGroup .Tab", function(event) {
    if (!$(this).hasClass("is-selected")) {
      $(this).siblings().removeClass("is-selected");
      $(this).addClass("is-selected");
      loadcontent = $(this).data("loadcontent");
      $("#table-content").load(loadcontent, function(){
        countUp();
      });
    }
  });
  
});

// Dropdown loader
$(document).on("click", ".Dropdown.ContentLoader .Dropdown-menuItemContent", function(event) {
  loadcontent = $(this).data("loadcontent");
  loadTable(loadcontent);
  // if (!$(this).hasClass("is-selected")) {
  //   $("#table-content").html($("#table-loading").html());
  //   loadcontent = $(this).data("loadcontent");
  //   $("#table-content").load(loadcontent, function(){
  //     countUp();
  //   });
  //   hideActionFooter();
  // }
});

function loadTable(page) {
  $("#table-content").html($("#table-loading").html());
  $("#table-content").load(page, function(){
    countUp();
  });
  hideActionFooter();
}

$(document).on("click", ".showtooltip", function(event) { 
  $(".Tooltip").removeClass("is-open");
  $(this).siblings(".Tooltip").addClass("is-open");
});

$(document).on("click", ".Tooltip-close", function(event) { 
  $(this).closest(".Tooltip").removeClass("is-open");
});

$(document).on("mouseover", ".showtooltip-hover", function(event) { 
  $(".Tooltip").removeClass("is-open");
  $(this).siblings(".Tooltip").addClass("is-open");
  $(this).closest("td").addClass("breakout");
});

$(document).on("mouseout", ".showtooltip-hover", function(event) { 
  $(".Tooltip").removeClass("is-open");
});

$(document).on("mouseover", ".tooltips-hover .Icon--featherTooltipCue", function(event) {
  $(".Tooltip").removeClass("is-open");
  $(this).siblings(".Tooltip").addClass("is-open");
  $(this).closest("th").addClass("breakout");
});

$(document).on("mouseout", ".tooltips-hover .Icon--featherTooltipCue", function(event) {
  $(".Tooltip").removeClass("is-open");
  $(this).closest("th").removeClass("breakout");
});

// Customize metrics modal tooltips hover
$(document).on("mouseover", ".metrics-list ul.tooltips-hover li label", function(event) {
  $(this).find(".Tooltip").addClass("is-open");
});

$(document).on("mouseout", ".metrics-list ul.tooltips-hover li label", function(event) {
  $(this).find(".Tooltip").removeClass("is-open");
});

// $(document).on("mouseover", ".tooltips-hover .column-tooltip", function(event) {
//   $(".Tooltip").removeClass("is-open");
//   $(this).siblings(".Tooltip").addClass("is-open");
//   $(this).closest("th").addClass("breakout");
// });
//
// $(document).on("mouseout", ".tooltips-hover .column-tooltip", function(event) {
//   $(".Tooltip").removeClass("is-open");
//   $(this).closest("th").removeClass("breakout");
// });

$(document).on("hover", ".ad-thumbnail", function(event) { 
  $(this).fadeTo(0.5);
});

$(document).on("click", ".adsmgr-leftnav li:not(.section)", function(event) { 
  $('html,body').scrollTop(0);
  $(this).siblings(".adsmgr-leftnav li").removeClass("selected");
  $(this).addClass("selected");
  $("#adsmgr-audience").load($(this).data("loadcontent"));
  hideActionFooter();
});

$(document).on("click", ".Token, .dropdown-menu .Button:first-child,.dropdown-menu .is-dropdown", function(event) {
  $(".Dropdown").hide(); 
  $(this).siblings(".Dropdown").show();
  event.stopPropagation();
});

$(document).on("click", ".filter-token .Dropdown", function(event) {
  event.stopPropagation();
});

$(document).on("click", ".Dropdown .Dropdown-menuItem", function(event) {
  if ($(this).siblings(".Dropdown-menuItem.is-selected").length > 0 && $(this).data("value")) { // Only select items if data attribute is not null
    $(this).siblings(".Dropdown-menuItem").removeClass("is-selected");
    $(this).addClass("is-selected"); 
    
    // Special behavior for deleting and setting  dropdown defaults, e.g., Customize metrics
    if ($(this).hasClass("default-eligible")) {
      // reset everything
      $(".set-as-default").remove();
      var setasdefaultlink = '<span class="set-as-default"> <a href="#">Set as default</a></span>';
      if (!$(this).hasClass("is-default")) {
        $(this).find(".Dropdown-menuItemContent").append(setasdefaultlink);
      }
    }
    
    var dropdowntext =  $(this).data("value");
    $(this).closest(".dropdown-menu").find(".dropdown-text").html(dropdowntext);
  }
});

// Special behavior for deleting and setting  dropdown defaults, e.g., Customize metrics
$(document).on("click", ".Dropdown .Dropdown-menuItem .dropdown-action.delete", function(event) {
  confirm("Delete this saved preset?");
  $(".Dropdown").hide(); 
  event.stopPropagation();
});  

$(document).on("click", "body", function(event) {
  $(".Dropdown").hide();
});

// $(document).on("click", ".ButtonGroup .Button", function(event) {
//   $(this).siblings(".Button").removeClass("is-selected");
//   $(this).addClass("is-selected");
// });

$(document).on("click", ".options-toggle-switch:not(.disabled)", function(event) {
  $(this).siblings(".more-options, .select-section").toggle();
  $(this).find(".FeatherDocs-expandableToggle").toggleClass("is-expanded");
});

// Check-all checkbox behavior
$(document).on("click", ".Table th.checkcol input:checkbox", function(event) {
  if ($(this).prop("checked")) {
    $(this).closest("table").find("input:checkbox").prop("checked", true);
    showActionFooter($(this).closest("table").data("actionfooter"));
  } else {
    $(this).closest("table").find("input:checkbox").prop("checked", false);
    hideActionFooter();
  }
});

// Individual checkbox behavior
$(document).on("click", ".Table td input:checkbox", function(event) {
  // Uncheck the check-all checkbox
  $(this).closest(".Table").find("th.checkcol input:checkbox").prop("checked", false);
  if ($(".Table input:checked").length == 1) {
    showActionFooter($(this).closest("table").data("actionfooter"));
  } else if ($(".Table input:checked").length == 0) {
    hideActionFooter();
  }
});

// Custom metrics select section
$(document).on("click", ".custom-metrics-container .select-section", function(event) {
  if ($(this).siblings(".more-options").find("input:checkbox:not(:checked)").length > 0) {
    $(this).siblings(".more-options").find("input:checkbox").prop("checked", true);
    $(this).html("unselect all");
  } else {
    $(this).siblings(".more-options").find("input:checkbox:not(:disabled)").prop("checked", false);
    $(this).html("select all");
  }
  var count = $(this).closest(".options-toggle").find("input:checked").length;
  $(this).closest(".options-toggle").find(".count").html(count);
});
$(document).on("click", ".custom-metrics-container .more-options input:checkbox", function(event) {
  if ($(this).closest(".more-options").find("input:checkbox:not(:checked)").length > 0) {
    $(this).closest(".options-toggle").find(".select-section").html("select all");
  } else {
    $(this).closest(".options-toggle").find(".select-section").html("unselect all");
  }
  var count = $(this).closest(".options-toggle").find("input:checked").length;
  $(this).closest(".options-toggle").find(".count").html(count);
});

function loadLevel(level, direction) {
  // Clear out filters when switching pages
  clearFilters();
  hideActionFooter();
  var margin1 = direction == "down" ? "-=40px" : "+=40px";
  var margin2 = direction == "down" ? "+=40px" : "-=40px";
  $("#subnav-content, #adsmgr-page-container").animate({
      marginLeft: margin1,
      opacity: 0
    }, 150, function(){
      $("#subnav-content, #adsmgr-page-container").css("margin-left", margin2);
      
      // Load the header
      $("#header-content").load("header-"+level+".html", function(){
        $("#adsmgr-page-container").animate({
          marginLeft: 0,
          opacity: 1
        }, 75);
      });

      // Load the subnav
      $("#subnav-content").load("subnav-"+level+".html", function(){
        $("#subnav-content").animate({
          marginLeft: 0,
          opacity: 1
        }, 150);
      });
      
      // Load the chart
      $("#chart-content").load("chart-"+level+".html", function(){
        $("#chart-content").animate({
          marginLeft: 0,
          opacity: 1
        }, 150);
        countUp();
      });
      
      // Load the table
      if (level == "campaign") {
        $("#table-content").load("table-campaign-adgroups.html", function(){
          countUp();
        });
      }
      switch(level) {
        case "fundingsource":
          $("#table-content").load("table-fundingsource-campaigns.html", function(){
            countUp();
          });
          break;
        case "campaign":
          $("#table-content").load("table-campaign-adgroups.html", function(){
            countUp();
          });
          break;
        case "adgroup":
          $("#table-content").load("table-campaign-ads.html", function(){
            countUp();
          });
          break;  
        default:
          $("#table-content").load("table-account-campaigns.html", function(){
            $(".filter-set-wrapper .filter-token").show();
            $("#filter-set-text").html("Filters: Default");
            $("#filter-set-create").hide();
            countUp();
          });
      }
      $('html,body').scrollTop(0);
    });
}

function showModal(content, callback) {
  if (content) {
    $("#modal-content").load("modal-" + content + ".html", function(){
      if (callback) {
        callback();
      }
    });
    $("body").addClass("overlay-on");
    $("#overlay-modal").show();
    $(document).keyup(function(e) {
      if (e.keyCode == 27) { // esc key
        hideModal();
      }
    });
  }
}

function hideModal() {
  $("#overlay-modal").fadeOut(100, function() {
    $("#modal-content").html('');
    $("body").removeClass("overlay-on");
  });
}

function showPanel(content, callback) {
  if (content) {	  
    $("#panel-content").load("panel-" + content + ".html", function(){
		  $(".Sheet-container").animate({
				marginRight: 0,
				opacity: 1
		  }, 250);
			content_height = $(".Sheet-container").height()-170;
			$(".Sheet-body").height(content_height);
      if (callback) {
        callback();
      }
    });
    $("#overlay-panel").fadeIn(100);
    $("body").addClass("overlay-on");
    $(document).keyup(function(e) {
      if (e.keyCode == 27) { // esc key
        hidePanel();
      }
    });
  }
  // Disable bg scrolling HACK
  $(".adsmgr-page-header").css("margin-top", "48px");
  var window_height = $( window ).height();
  $("#root").css("height", (window_height) + "px").css("overflow", "hidden");
}

function hidePanel() {
  $(".Sheet-container").animate({
		marginRight: -200
  }, 250);
  $("#overlay-panel").fadeOut(100, function() {
    $("#panel-content").html('');
  });
  $("body").removeClass("overlay-on");
  
  // Re-enable bg scrolling HACK
  $(".adsmgr-page-header").css("margin-top", "-92px");
  $("#root").css("height", "auto").css("overflow", "visible");
}

function selectFormField() {
  $("input.selectme").select();
}

function showActionFooter(content) {
  if (content) {
    $(".action-footer").addClass("show");
    $(".action-footer-content").load("actionfooter-" + content + ".html");
  }
}

function clearFilters() {
  $(".filter-set-wrapper .filter-token").hide();
  $("#filter-set-text").html("Filters: Custom");
  $(".dropdown-menu.filter-dropdown .Dropdown-menuItem").removeClass("is-selected");
  $(".dropdown-menu.filter-dropdown .dropdown-text").html("");
  var header_bottom = $("#adsmgr-page-header").offset().top + $("#adsmgr-page-header").outerHeight();
  $(".Table").stickyTableHeaders({fixedOffset: header_bottom});
}

function hideActionFooter() {
  $(".action-footer").removeClass("show");
}

function countUp() {
  $(".count-to").each(function(){
    countto = $(this).data("countto");
    var duration = (countto / 100) * 600;
    $(this).html(countto);
    // $(this).countTo({
    //     from: 0,
    //     to: countto,
    //     speed: duration,
    //     refreshInterval: 10,
    //     onComplete: function(value) {
    //         //console.debug(this);
    //     }
    // });
    $(this).siblings(".progress-bar").find(".bar.top").animate({
      width: countto + "%"
    }, duration);
  });
}
