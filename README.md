# README #

This repository is for the Ads Manager prototype

### What is this repository for? ###

This prototype is an static HTML/CSS/JS prototype for Twitter Ads Manager, used to test ideas and communicate design among the team

### How do I get set up? ###

There's no backend, it's all just static HTML/CSS/JS, so just access it via a local web server 


### Who do I talk to? ###

Andy Yang